from django.urls import path
from . import views

app_name = 'mypage'

urlpatterns = [
    path('', views.home, name='home'),
    path('skill/', views.skill, name='skill'),
    path('workplay/', views.workplay, name='workplay'),
    path('message/', views.message, name='message'),
    path('thankyou/', views.ty, name='ty'),

    path('form/', views.form, name='form'),
    path('show/', views.show, name='show'),
    path('delete-activity/<int:pk>/', views.delete_activity, name='delete_activity'),
]
