from django import forms
from .models import ScheduleModel
import datetime


class ScheduleForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs={
            "placeholder": "Project's Title",
        }
    ))
    category = forms.CharField(widget=forms.TextInput(
        attrs={
            "placeholder": "Project's Category",
        }
    ))
    date = forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(), widget=forms.TextInput(attrs=
    {
        'class': 'form-control'
    }))

    time = forms.TimeField(input_formats=['%H:%M:%S'], initial=datetime.time, widget=forms.TextInput(attrs=
    {
        'class': 'form-control'
    }))

    class Meta:
        model = ScheduleModel
        fields = [
            'date',
            'time',
            'name',
            'category',
        ]
