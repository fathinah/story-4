from django.db import models


class ScheduleModel(models.Model):
    date = models.DateField()
    time = models.TimeField(auto_now_add=False)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=50)

    def __str__(self):
        return "{}.{}".format(self.id, self.name)


