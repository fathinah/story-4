from django.shortcuts import render, get_object_or_404, redirect
from .forms import ScheduleForm
from .models import ScheduleModel
from django.contrib import messages


def home(request):
    return render(request, 'home.html')


def skill(request):
    return render(request, 'skill.html')


def workplay(request):
    return render(request, 'workplay.html')


def message(request):
    return render(request, 'message.html')


def ty(request):
    return render(request, 'ty.html')


def show(request):
    data = ScheduleModel.objects.all()
    return render(request, 'result.html', {'data': data})


def form(request):
    if request.method == "POST":
        data = ScheduleForm(request.POST)
        if data.is_valid():
            data.save()
            return redirect('mypage:show')
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')

    else:
        data = ScheduleForm()


    return render(request, 'form.html', {'form': data})


def delete_activity(request, pk):
    activity = get_object_or_404(ScheduleModel, pk=pk)
    activity.delete()


    return redirect('mypage:show')